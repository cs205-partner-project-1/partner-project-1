"""Campus model"""

from building import Building

class Campus:
    """Campus wrapper class for professor, building, and class"""
    def __init__(self, name: str):
        """Initialize cmapus, just needs a name as components are individually added.

        Args:
            name (str): name of campus
        """
        self.name = name
        self.buildings = set()
        self.classes = set()

    def add_building(self, building: Building):
        """Add a building to the campus

        Args:
            building (Building): building object
        """
        self.buildings.add(building)
        relative_classes = building.classes
        for _class in relative_classes:
            self.classes.add(_class)

    def remove_building(self, building: Building):
        """Remove a building from the campus

        Args:
            building (Building): building object
        """
        self.buildings.remove(building)
        classes_to_remove = building.classes
        for _class in classes_to_remove:
            self.classes.remove(_class)

    def find_building(self, name: str):
        """Find a building in the given set by name.

        Args:
            name (str): name of building

        Returns:
            Union[None, str]: return None if not found otherwise return the building object
        """
        for building in self.buildings:
            if(building.name == name):
                return building
        # not found
        return None

    def close(self):
        """Close the campus, removing all data."""
        self.buildings.clear()
        self.classes.clear()

    def close_incorrect(self):
        """An incorrect function to showcase testing."""
        print("No more campus byby")

    def __str__(self):
        return f'Campus of {self.name} with {len(self.buildings)} buildings and a total of {len(self.classes)} classes.'

    def __eq__(self, other):
        return self.name == other.name and self.buildings == other.buildings and self.classes == self.classes
