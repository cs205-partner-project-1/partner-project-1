"""Class model"""

from professor import Professor


class _Class:
    """Entity to represent a class on campus."""
    def __init__(self, name: str, professor: Professor, location: str, num_students: int):
        """Initialize instance of class entity.

        Args:
            name (str): name of class
            professor (Professor): professor as class variable
            location (str): where the class is located
            num_students (int): number of students on campus
        """
        self.name = name
        self.professor = professor
        self.location = location
        self.num_students = num_students

    def __str__(self):
        return f'Class {self.name} located in {self.location} with {self.num_students} students.'

    def __eq__(self, other):
        return self.name == other.name and self.professor == other.professor and \
               self.location == other.location and self.num_students == other.num_students

    def __hash__(self):
        return hash((self.name, self.professor, self.location, self.num_students))
