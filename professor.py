"""Professor model"""
from enum import Enum


class Department(Enum):
    """Department Enum for ease."""
    MATH = 'math'
    ART = 'art'
    MUSIC = 'music'
    ENGLISH = 'english'
    ENGINEERING = 'engineering'
    CHEMISTRY = 'chemistry'
    PHYSICS = 'physics'
    BIOLOGY = 'biology'
    COMPUTER_SCIENCE = 'computer science'


class Professor:
    """Professor class model."""
    def __init__(self, name: str, department: Department, salary: int):
        """Initialize a professor instance

        Args:
            name (str): name of professor
            department (Department): department the professor teaches for
            salary (int): salary
        """
        self.name = name
        self.department = department
        self.salary = salary

    def is_professor_paid_enough(self):
        if self.salary < 100000:
            return False
        else:
            return True

    def __str__(self):
        return f'Professor {self.name} works in department of {self.department} and makes ${self.salary} a year.'

    def __eq__(self, other):
        return self.name == other.name and self.department == other.department and self.salary == other.salary

    def __hash__(self):
        return hash((self.name, self.department, self.salary))
