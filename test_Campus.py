import unittest
from _class import _Class
from campus import Campus
from building import Building
from professor import Professor
from professor import Department

campus = None

class TestCampus(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        print('setUpClass()')
        cls.campus = Campus("UVM")
        # create some buildings, classes, and professors
        cls.building1 = Building("Innovation", "999 South Prospect")
        cls.building2 = Building("Davis Center", "888 Main Street")
        cls.building3 = Building("Hills", "666 Bees&Maggots")
        cls.professor1 = Professor("Dr.Strange", Department.CHEMISTRY, 70000)
        cls.professor2 = Professor("Dr.GreenMtMan", Department.ENGLISH, 70000)
        cls.professor3 = Professor("Dr.MathMan", Department.MATH, 70000)
        cls.class1 = _Class("Intro to chemicals", cls.professor1, "Innovation 205", 25)
        cls.class2 = _Class("Mountians for monkeys", cls.professor2, "Davis Center Green", 40)
        cls.class3 = _Class("The math aint mathin so heres how to do it", cls.professor3, "Hills 224", 90)

        cls.building1.add_class(cls.class1)
        cls.building2.add_class(cls.class2)
        cls.building3.add_class(cls.class3)

        cls.campus.add_building(cls.building1)
        cls.campus.add_building(cls.building2)
        cls.campus.add_building(cls.building3)

    @classmethod
    def tearDownClass(cls):
        print('tearDownClass()')

    def setUp(self):
        print('setUp()')

    def tearDown(self):
        print('tearDown()')
        self.campus.close()

    def test_close_correct(self):
        # tests the correct implimentation of close()
        self.campus.add_building(self.building1)
        self.campus.close()
        self.assertEqual(self.campus.buildings, set())
        self.assertEqual(self.campus.classes, set())

    def test_close_incorrect(self):
        # tests the incorrect implimentation of close(), this test will fail
        self.campus.add_building(self.building1)
        self.campus.close_incorrect()
        self.assertEqual(self.campus.buildings, set())
        self.assertEqual(self.campus.classes, set())

    def test_find_building_one(self):
        # check that search for building that does not exist returns None
        self.assertEqual(self.campus.find_building("lala"), None)

    def test_find_building_two(self):
        # check that search for building that does exist returns the correct building
        self.campus.add_building(self.building1)
        self.assertEqual(self.campus.find_building("Innovation"), self.building1)

    def test_find_building_three(self):
        # check that search for building that was just removed from campus returns None
        self.campus.add_building(self.building1)
        self.campus.remove_building(self.building1)
        self.assertEqual(self.campus.find_building("Innovation"), None)

    def test_add_building_one(self):
        # test that a new building is actually added
        newbuilding = Building("da new building", "da moon")
        self.campus.add_building(newbuilding)
        self.assertTrue(newbuilding in self.campus.buildings)


if __name__ == '__main__':
    unittest.main()
