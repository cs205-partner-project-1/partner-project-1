"""Building model"""
from _class import _Class

class Building:
    """Entity to represent a building on a campus."""
    def __init__(self, name: str, address: str):
        """Initialize building instance.

        Args:
            name (str): name of building
            address (str): address of building
        """
        self.name = name
        self.address = address
        self.classes = set()

    def add_class(self, a_class: _Class):
        """Add a class to the building (wrapper because a set)

        Args:
            a_class (_class): a _Class object from _class
        """
        self.classes.add(a_class)

    def __str__(self):
        return f'{self.name} building located at {self.address} with {len(self.classes)} classes'

    def __eq__(self, other):
        return self.name == other.name and self.address == other.address and self.classes == other.classes

    def __hash__(self):
        return hash((self.name, self.address))
